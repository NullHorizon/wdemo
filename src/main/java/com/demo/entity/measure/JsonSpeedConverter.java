package com.demo.entity.measure;

import com.google.gson.Gson;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class JsonSpeedConverter implements AttributeConverter<Speed, String> {

    @Override
    public String convertToDatabaseColumn(Speed speed) {
        return new Gson().toJson(speed, Measure.class);
    }

    @Override
    public Speed convertToEntityAttribute(String s) {
        return new Gson().fromJson(s, Speed.class);
    }
}
