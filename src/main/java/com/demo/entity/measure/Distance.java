package com.demo.entity.measure;

import java.util.HashMap;

public class Distance extends Measure {

    public Distance() {}

    public Distance(double value, String unit) {
        super(value, unit);
    }

    @Override
    HashMap<String, Double> getAvailableUnits() {
        return new HashMap<>() {{
            put("M", 1.0);
            put("KM", 1000.0);
        }};
    }

    @Override
    public int compareTo(Measure measure) {
        if (measure instanceof Distance) {
            Distance s = (Distance)measure;
            return (Double.compare(s.getNormalized(), this.getNormalized()));
        } else {
            return -1;
        }
    }
}
