package com.demo.entity.measure;

import javax.persistence.Embeddable;
import java.util.HashMap;

@Embeddable
public abstract class Measure implements Comparable<Measure> {
    private String unit;
    private double value;
    abstract HashMap<String, Double> getAvailableUnits();

    public Measure() {}

    public Measure(double value, String unit) {
        this.setUnit(unit);
        this.setValue(value);
    }

    @Override
    public String toString() {
        return value + " " + unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) throws RuntimeException {
        if (!this.getAvailableUnits().containsKey(unit.toUpperCase())) {
            throw new RuntimeException("Invalid unit");
        }
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    protected double getNormalized() {
        return this.getAvailableUnits().get(this.getUnit()) * this.getValue();
    }
}
