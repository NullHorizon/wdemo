package com.demo.entity.measure;

import com.google.gson.Gson;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class JsonDistanceConverter implements AttributeConverter<Distance, String> {

    @Override
    public String convertToDatabaseColumn(Distance distance) {
        return new Gson().toJson(distance, Measure.class);
    }

    @Override
    public Distance convertToEntityAttribute(String s) {
        return new Gson().fromJson(s, Distance.class);
    }
}
