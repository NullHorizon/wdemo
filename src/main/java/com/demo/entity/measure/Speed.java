package com.demo.entity.measure;

import java.util.HashMap;

public class Speed extends Measure {

    public Speed() {}
    public Speed(double value, String unit) {
        super(value, unit);
    }

    @Override
    HashMap<String, Double> getAvailableUnits() {
        return new HashMap<>() {{
            put("MPS", 1.0);
            put("KMPS", 1000.0);
        }};
    }

    @Override
    public int compareTo(Measure measure) {
        if (measure instanceof Speed) {
            Speed s = (Speed)measure;
            return (Double.compare(s.getNormalized(), this.getNormalized()));
        } else {
            return -1;
        }
    }
}
