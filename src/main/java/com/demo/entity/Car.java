package com.demo.entity;

import com.demo.entity.measure.JsonSpeedConverter;
import com.demo.entity.measure.Speed;

import javax.persistence.*;

@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carSeqGen")
    @SequenceGenerator(name="carSeqGen", sequenceName = "carSeq")
    private Long id;
    @JoinColumn(name = "trackId", nullable = false)
    private Long trackid;
    @Column(name = "code", nullable = false)
    private String code;
    @Column(name = "transmission", nullable = false)
    private String transmission;
    @Column(name = "ai", nullable = false)
    private String ai;
    @Convert(converter = JsonSpeedConverter.class)
    @Column(name = "maxspeed", nullable = false)
    private Speed maxSpeed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTrackId() {
        return trackid;
    }

    public void setTrackId(Long trackId) {
        this.trackid = trackId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getAi() {
        return ai;
    }

    public void setAi(String ai) {
        this.ai = ai;
    }

    public Speed getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Speed maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
