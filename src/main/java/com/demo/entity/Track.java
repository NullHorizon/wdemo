package com.demo.entity;

import com.demo.entity.measure.Distance;
import com.demo.entity.measure.JsonDistanceConverter;

import javax.persistence.*;

@Entity
@Table(name = "tracks")
public class Track {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trackSeqGen")
    @SequenceGenerator(name="trackSeqGen", sequenceName = "trackSeq")
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", nullable = false)
    private String description;
    @Convert(converter = JsonDistanceConverter.class)
    @Column(name = "length", nullable = false)
    private Distance length;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Distance getLength() {
        return length;
    }

    public void setLength(Distance length) {
        this.length = length;
    }
}
