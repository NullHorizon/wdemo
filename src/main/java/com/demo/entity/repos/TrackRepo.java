package com.demo.entity.repos;

import com.demo.entity.Track;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepo extends JpaRepository<Track, Long> {
}
