package com.demo.entity.repos;

import com.demo.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CarRepo extends JpaRepository<Car, Long> {
    @Query("Select car from Car car WHERE car.trackid = :trackid")
    List<Car> findAllByTrackId(@Param("trackid") long trackid);
}
