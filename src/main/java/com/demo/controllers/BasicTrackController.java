package com.demo.controllers;

import com.demo.entity.Track;
import com.demo.entity.measure.Distance;
import com.demo.entity.repos.TrackRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Component
@RestController
@RequestMapping(path = "/tracks")
public class BasicTrackController {
    @Autowired
    TrackRepo trackRepo;

    @RequestMapping(method = RequestMethod.GET,
            value = "/get/{id}")
    public Track getSavedTrack(@PathVariable long id) {
        if (!trackRepo.findById(id).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + id + " not found.");
        }
        return new Track();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/get")
    public List<Track> getAllSavedTracks() {
        return trackRepo.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/createTest")
    public Track createTrack() {
        Track track1 = new Track();
        track1.setDescription("descr");
        track1.setLength(new Distance(1.1, "KM"));
        track1.setName("name");
        return trackRepo.save(track1);
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/save")
    public Track saveTrack(@RequestBody Track track) {
        return trackRepo.save(track);
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/delete/{id}")
    public String deleteSavedTrack(@PathVariable long id) {
        if (!trackRepo.findById(id).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + id + " not found.");
        }
        trackRepo.delete(trackRepo.findById(id).get());
        return "OK";
    }
}
