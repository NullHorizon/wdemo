package com.demo.controllers;

import com.demo.entity.Car;
import com.demo.entity.measure.Speed;
import com.demo.entity.repos.CarRepo;
import com.demo.entity.repos.TrackRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@RestController
@RequestMapping(path = "/cars")
public class BasicCarController {
    @Autowired
    CarRepo carRepo;
    @Autowired
    TrackRepo trackRepo;

    @RequestMapping(method = RequestMethod.GET,
            value = "/{trackid}/get")
    public List<Car> getAllCarsForTrack(@PathVariable long trackid) {
        if (!trackRepo.findById(trackid).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + trackid + " not found.");
        }
        return carRepo.findAllByTrackId(trackid);
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/{trackid}/get/{carid}")
    public Car getCarForTrack(@PathVariable long trackid, @PathVariable long carid) {
        if (!trackRepo.findById(trackid).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + trackid + " not found.");
        }
        if (!carRepo.findById(carid).isPresent()) {
            throw new ResourceNotFoundException("Car with id = " + carid + " not found.");
        }
        return carRepo.findById(carid).get();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/{trackid}/createTest")
    public Car getCarForTrack(@PathVariable long trackid) {
        if (!trackRepo.findById(trackid).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + trackid + " not found.");
        }
        Car car = new Car();
        car.setAi("enabled");
        car.setCode("code");
        car.setMaxSpeed(new Speed(1.1, "mps"));
        car.setTransmission("transmission");
        car.setTrackId(trackid);
        return carRepo.save(car);
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/{trackid}/save")
    public Car saveCar(@PathVariable long trackid, @RequestBody Car car) {
        if (!trackRepo.findById(trackid).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + trackid + " not found.");
        }
        return carRepo.save(car);
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/{trackid}/delete/{carid}")
    public String deleteSavedTrack(@PathVariable long trackid, @PathVariable long carid) {
        if (!trackRepo.findById(trackid).isPresent()) {
            throw new ResourceNotFoundException("Track with id = " + trackid + " not found.");
        }
        if (!carRepo.findById(carid).isPresent()) {
            throw new ResourceNotFoundException("Car with id = " + carid + " not found.");
        }
        carRepo.deleteById(carid);
        return "OK";
    }
}
