package com.demo;

import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {
    @Bean
    public ServletRegistrationBean<SpringVaadinServlet> springVaadinServlet() {
        SpringVaadinServlet servlet = new SpringVaadinServlet();
        ServletRegistrationBean<SpringVaadinServlet> registrationBean = new ServletRegistrationBean<>(servlet, "/ui/*");
        registrationBean.setLoadOnStartup(1);
        registrationBean.setName("VaadinServlet");
        return registrationBean;
    }
}
