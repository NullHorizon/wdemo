package com.demo.ui;

import com.demo.entity.Car;
import com.demo.entity.Track;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Theme("valo")
@SpringUI
@Push(PushMode.MANUAL)
public class MainUI extends UI {

    @Value("${server.port}")
    private String backServerPort;

    private Grid<Track> tracks = new Grid<>();
    private Grid<Car> cars = new Grid<>();

    private Window createTrackWindow = new Window("Create Track");
    private Window createCarWindow = new Window("Create Car");
    private Window addFromJsonWindow = new Window("Add from Json");

    private Button createTrackBtn = new Button("Create Track");
    private Button createCarBtn = new Button("Create Car");
    private Button addFromJson = new Button("Add from JSON");

    private TextField trackNameField = new TextField("Name");
    private TextField trackLengthValueField = new TextField("Length");
    private ComboBox<String> trackLengthUnitCB = new ComboBox<>("Unit");
    private TextField trackDescriptionField = new TextField("Description");
    private Button createTrackWindowBtn = new Button("Create Track");
    private Label errorMsgTrackWindowLabel = new Label();

    private TextField carCodeField = new TextField("Code");
    private TextField carSpeedValueField = new TextField("Max-Speed");
    private ComboBox<String> carSpeedUnitCB = new ComboBox<>("Unit");
    private ComboBox<String> carAICB = new ComboBox<>("AI");
    private ComboBox<String> carTransmissionCB = new ComboBox<>("Transmission");
    private Button createCarWindowBtn = new Button("Create Car");
    private Label errorMsgCarWindowLabel = new Label();

    private TextArea jsonTextArea = new TextArea("Json");
    private Button loadJsonBtn = new Button("Load JSON");

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        trackLengthUnitCB.setItems(new ArrayList<>() {{
            add("KM");
            add("M");
        }});

        carSpeedUnitCB.setItems(new ArrayList<>() {{
            add("KMPS");
            add("MPS");
        }});

        carAICB.setItems(new ArrayList<>() {{
            add("ENABLED");
            add("DISABLED");
        }});

        carTransmissionCB.setItems(new ArrayList<>() {{
            add("AUTOMATIC");
            add("MANUAL");
        }});

        tracks.addColumn(Track::getId).setCaption("ID");
        tracks.addColumn(Track::getName).setCaption("Name");
        tracks.addColumn(Track::getLength).setCaption("Length");
        tracks.addColumn(Track::getDescription).setCaption("Description");

        tracks.addSelectionListener((SelectionListener<Track>) selectionEvent -> refreshCars());

        cars.addColumn(Car::getId).setCaption("ID");
        cars.addColumn(Car::getCode).setCaption("Code");
        cars.addColumn(Car::getMaxSpeed).setCaption("MaxSpeed");
        cars.addColumn(Car::getAi).setCaption("AI");
        cars.addColumn(Car::getTransmission).setCaption("Transmission");

        refreshTracks();

        VerticalLayout createTrackLayout = new VerticalLayout();
        createTrackLayout.addComponent(trackNameField);
        HorizontalLayout lengthCBLayout = new HorizontalLayout();
        lengthCBLayout.addComponent(trackLengthValueField);
        lengthCBLayout.addComponent(trackLengthUnitCB);
        createTrackLayout.addComponent(lengthCBLayout);
        createTrackLayout.addComponent(trackDescriptionField);
        createTrackLayout.addComponent(createTrackWindowBtn);
        createTrackLayout.addComponent(errorMsgTrackWindowLabel);

        createTrackWindow.setContent(createTrackLayout);
        createTrackBtn.addClickListener((Button.ClickListener) clickEvent -> {
            if (!createTrackWindow.isAttached()) {
                addWindow(createTrackWindow);
            }
        });

        VerticalLayout createCarLayout = new VerticalLayout();
        createCarLayout.addComponent(carCodeField);
        HorizontalLayout speedCBLayout = new HorizontalLayout();
        speedCBLayout.addComponent(carSpeedValueField);
        speedCBLayout.addComponent(carSpeedUnitCB);
        createCarLayout.addComponent(speedCBLayout);
        createCarLayout.addComponent(carAICB);
        createCarLayout.addComponent(carTransmissionCB);
        createCarLayout.addComponent(createCarWindowBtn);
        createCarLayout.addComponent(errorMsgCarWindowLabel);

        createCarWindow.setContent(createCarLayout);
        createCarBtn.addClickListener((Button.ClickListener) clickEvent -> {
            if (!createCarWindow.isAttached()) {
                addWindow(createCarWindow);
            }
        });

        VerticalLayout addFromJsonLayout = new VerticalLayout();
        addFromJsonLayout.addComponent(jsonTextArea);
        addFromJsonLayout.addComponent(loadJsonBtn);

        loadJsonBtn.addClickListener((Button.ClickListener) clickEvent -> {
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject= ((JsonObject)jsonParser
                    .parse(jsonTextArea.getValue()));
            JsonArray jsonArray = jsonObject.getAsJsonArray("tracks");
            Track[] generatedTracks = new Gson()
                    .fromJson(jsonArray, Track[].class);
            // TODO: separate car\track entites and save with REST
            // TODO: drop save log to Label
            //tracks.setItems(generatedTracks);
        });

        addFromJsonWindow.setContent(addFromJsonLayout);
        addFromJson.addClickListener((Button.ClickListener) clickEvent -> {
            if (!addFromJsonWindow.isAttached()) {
                addWindow(addFromJsonWindow);
            }
        });

        HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.addComponent(tracks);
        mainLayout.addComponent(cars);
        mainLayout.addComponent(createTrackBtn);
        mainLayout.addComponent(createCarBtn);
        mainLayout.addComponent(addFromJson);

        setContent(mainLayout);
    }

    private void refreshTracks() {
        RestTemplate restTemplate = new RestTemplate();
        String req = "http://localhost:" + backServerPort + "/tracks/get";
        ResponseEntity<List<Track>> responseEntity = restTemplate.exchange(req, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Track>>() {
                });
        tracks.setItems(responseEntity.getBody());
    }

    private void refreshCars() {
        if (!tracks.getSelectionModel().getFirstSelectedItem().isPresent()) {
            return;
        }
        long trackid = tracks.getSelectionModel().getFirstSelectedItem().get().getId();
        RestTemplate restTemplate = new RestTemplate();
        String req = "http://localhost:" + backServerPort + "/cars/" + trackid + "/get";
        ResponseEntity<List<Car>> responseEntity = restTemplate.exchange(req, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Car>>() {
                });
        cars.setItems(responseEntity.getBody());
    }

}

